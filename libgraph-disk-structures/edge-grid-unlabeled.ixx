#include <libgraph-disk-structures/detail/utility.hxx>

namespace graph_disk_structures
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto edge_grid<void, Alloc, PAGE_SIZE>::create(uint32_t n, uint32_t block_size,
                                               Alloc<PAGE_SIZE>* alloc)
    -> header
{
    uint32_t n_grid = std::ceil(n / static_cast<float>(block_size));

    header h = {};
    h.grid_header = grid::create(n_grid * n_grid, alloc);
    h.n_grid = n_grid;
    h.block_size = block_size;
    h.n = n;
    h.m = 0;

    for(uint32_t u_id = 0; u_id < n_grid; ++u_id)
    {
        for(uint32_t v_id = 0; v_id < n_grid; ++v_id)
        {
            auto set_header = set::create(alloc);
            grid::write(h.grid_header, detail::idx(u_id, v_id, n_grid),
                        set_header, alloc);
        }
    }

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void edge_grid<void, Alloc, PAGE_SIZE>::destroy(header* h,
                                                Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t u_id = 0; u_id < h->n_grid; ++u_id)
    {
        for(uint32_t v_id = 0; v_id < h->n_grid; ++v_id)
        {
            auto set_header = grid::read(
                h->grid_header, detail::idx(u_id, v_id, h->n_grid), *alloc);
            set::destroy(&set_header, alloc);
        }
    }

    grid::destroy(&h->grid_header, alloc);
    h->n_grid = 0;
    h->block_size = 0;
    h->n = 0;
    h->m = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void edge_grid<void, Alloc, PAGE_SIZE>::clear(header* h,
                                              Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t u_id = 0; u_id < h->n_grid; ++u_id)
    {
        for(uint32_t v_id = 0; v_id < h->n_grid; ++v_id)
        {
            auto set_header = grid::read(
                h->grid_header, detail::idx(u_id, v_id, h->n_grid), *alloc);
            set::clear(&set_header, alloc);
            grid::write(h->grid_header, detail::idx(u_id, v_id, h->n_grid),
                        set_header, alloc);
        }
    }

    h->m = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void edge_grid<void, Alloc, PAGE_SIZE>::insert_edge(header* h, uint32_t u,
                                                    uint32_t v,
                                                    Alloc<PAGE_SIZE>* alloc)
{
    uint32_t i = detail::idx(u / h->block_size, v / h->block_size, h->n_grid);
    auto set_header = grid::read(h->grid_header, i, *alloc);

    if(set::insert(&set_header, {u, v}, alloc))
    {
        ++h->m;
        grid::write(h->grid_header, i, set_header, alloc);
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void edge_grid<void, Alloc, PAGE_SIZE>::delete_edge(header* h, uint32_t u,
                                                    uint32_t v,
                                                    Alloc<PAGE_SIZE>* alloc)
{
    uint32_t i = detail::idx(u / h->block_size, v / h->block_size, h->n_grid);
    auto set_header = grid::read(h->grid_header, i, *alloc);

    if(set::remove(&set_header, {u, v}, alloc))
    {
        --h->m;
        grid::write(h->grid_header, i, set_header, alloc);
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void edge_grid<void, Alloc, PAGE_SIZE>::delete_out_neighbors(
    header* h, uint32_t u, Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t v_id = 0; v_id < h->n_grid; ++v_id)
    {
        uint32_t i = detail::idx(u / h->block_size, v_id, h->n_grid);
        auto set_header = grid::read(h->grid_header, i, *alloc);

        std::vector<edge> to_remove;
        for(auto it = set::begin(set_header, *alloc);
            it != set::end(set_header, *alloc); ++it)
        {
            if(it->u == u)
            {
                to_remove.push_back(*it);
            }
        }

        if(!to_remove.empty())
        {
            for(const auto& key: to_remove)
            {
                set::remove(&set_header, key, alloc);
            }
            h->m -= to_remove.size();
            grid::write(h->grid_header, i, set_header, alloc);
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void edge_grid<void, Alloc, PAGE_SIZE>::delete_in_neighbors(
    header* h, uint32_t v, Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t u_id = 0; u_id < h->n_grid; ++u_id)
    {
        uint32_t i = detail::idx(u_id, v / h->block_size, h->n_grid);
        auto set_header = grid::read(h->grid_header, i, *alloc);

        std::vector<edge> to_remove;
        for(auto it = set::begin(set_header, *alloc);
            it != set::end(set_header, *alloc); ++it)
        {
            if(it->v == v)
            {
                to_remove.push_back(*it);
            }
        }

        if(!to_remove.empty())
        {
            for(const auto& key: to_remove)
            {
                set::remove(&set_header, key, alloc);
            }
            h->m -= to_remove.size();
            grid::write(h->grid_header, i, set_header, alloc);
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto edge_grid<void, Alloc, PAGE_SIZE>::has_edge(const header& h, uint32_t u,
                                                 uint32_t v,
                                                 const Alloc<PAGE_SIZE>& alloc)
    -> bool
{
    uint32_t i = detail::idx(u / h.block_size, v / h.block_size, h.n_grid);
    auto set_header = grid::read(h.grid_header, i, alloc);

    return set::find(set_header, {u, v}, alloc) != set::end(set_header, alloc);
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto edge_grid<void, Alloc, PAGE_SIZE>::out_neighbors(
    const header& h, uint32_t u, const Alloc<PAGE_SIZE>& alloc) -> vertex_stream
{
    for(uint32_t v_id = 0; v_id < h.n_grid; ++v_id)
    {
        uint32_t i = detail::idx(u / h.block_size, v_id, h.n_grid);
        auto set_header = grid::read(h.grid_header, i, alloc);

        for(auto it = set::begin(set_header, alloc);
            it != set::end(set_header, alloc); ++it)
        {
            if(it->u == u)
            {
                co_yield static_cast<uint32_t>(it->v);
            }
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto edge_grid<void, Alloc, PAGE_SIZE>::in_neighbors(
    const header& h, uint32_t v, const Alloc<PAGE_SIZE>& alloc) -> vertex_stream
{
    for(uint32_t u_id = 0; u_id < h.n_grid; ++u_id)
    {
        uint32_t i = detail::idx(u_id, v / h.block_size, h.n_grid);
        auto set_header = grid::read(h.grid_header, i, alloc);

        for(auto it = set::begin(set_header, alloc);
            it != set::end(set_header, alloc); ++it)
        {
            if(it->v == v)
            {
                co_yield static_cast<uint32_t>(it->u);
            }
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto edge_grid<void, Alloc, PAGE_SIZE>::edges(const header& h,
                                              const Alloc<PAGE_SIZE>& alloc)
    -> edge_stream
{
    for(uint32_t u_id = 0; u_id < h.n_grid; ++u_id)
    {
        for(uint32_t v_id = 0; v_id < h.n_grid; ++v_id)
        {
            uint32_t i = detail::idx(u_id, v_id, h.n_grid);
            auto set_header = grid::read(h.grid_header, i, alloc);

            for(auto it = set::begin(set_header, alloc);
                it != set::end(set_header, alloc); ++it)
            {
                co_yield {it->u, it->v};
            }
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto edge_grid<void, Alloc, PAGE_SIZE>::out_degree(
    const header& h, uint32_t u, const Alloc<PAGE_SIZE>& alloc) -> uint32_t
{
    uint32_t degree = 0;
    for(const auto& v: out_neighbors(h, u, alloc))
    {
        ++degree;
    }

    return degree;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto edge_grid<void, Alloc, PAGE_SIZE>::in_degree(const header& h, uint32_t v,
                                                  const Alloc<PAGE_SIZE>& alloc)
    -> uint32_t
{
    uint32_t degree = 0;
    for(const auto& u: in_neighbors(h, v, alloc))
    {
        ++degree;
    }

    return degree;
}

} // namespace graph_disk_structures
