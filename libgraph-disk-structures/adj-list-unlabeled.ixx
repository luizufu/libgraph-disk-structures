#include <iostream>

namespace graph_disk_structures
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<void, Alloc, PAGE_SIZE>::create(uint32_t n,
                                              Alloc<PAGE_SIZE>* alloc) -> header
{
    header h = {};
    h.vector_header = vector::create(n, alloc);
    h.n = n;
    h.m = 0;

    for(uint32_t u = 0; u < n; ++u)
    {
        auto set_header = set::create(alloc);
        vector::write(h.vector_header, u, set_header, alloc);
    }

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<void, Alloc, PAGE_SIZE>::destroy(header* h,
                                               Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t u = 0; u < h->n; ++u)
    {
        auto set_header = vector::read(h->vector_header, u, *alloc);
        set::destroy(&set_header, alloc);
    }

    vector::destroy(&h->vector_header, alloc);
    h->n = 0;
    h->m = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<void, Alloc, PAGE_SIZE>::clear(header* h, Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t u = 0; u < h->n; ++u)
    {
        auto set_header = vector::read(h->vector_header, u, *alloc);
        set::clear(&set_header, alloc);
        vector::write(h->vector_header, u, set_header, alloc);
    }

    h->m = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<void, Alloc, PAGE_SIZE>::insert_edge(header* h, uint32_t u,
                                                   uint32_t v,
                                                   Alloc<PAGE_SIZE>* alloc)
{
    auto set_header = vector::read(h->vector_header, u, *alloc);

    if(set::insert(&set_header, v, alloc))
    {
        ++h->m;
        vector::write(h->vector_header, u, set_header, alloc);
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<void, Alloc, PAGE_SIZE>::delete_edge(header* h, uint32_t u,
                                                   uint32_t v,
                                                   Alloc<PAGE_SIZE>* alloc)
{
    auto set_header = vector::read(h->vector_header, u, *alloc);

    if(set::remove(&set_header, v, alloc))
    {
        --h->m;
        vector::write(h->vector_header, u, set_header, alloc);
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<void, Alloc, PAGE_SIZE>::delete_out_neighbors(
    header* h, uint32_t u, Alloc<PAGE_SIZE>* alloc)
{
    auto set_header = vector::read(h->vector_header, u, *alloc);

    if(set_header.size > 0)
    {
        h->m -= set_header.size;
        set::clear(&set_header, alloc);
        vector::write(h->vector_header, u, set_header, alloc);
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<void, Alloc, PAGE_SIZE>::delete_in_neighbors(
    header* h, uint32_t v, Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t u = 0; u < h->n; ++u)
    {
        auto set_header = vector::read(h->vector_header, u, *alloc);
        if(set::remove(&set_header, v, alloc))
        {
            --h->m;
            vector::write(h->vector_header, u, set_header, alloc);
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<void, Alloc, PAGE_SIZE>::has_edge(const header& h, uint32_t u,
                                                uint32_t v,
                                                const Alloc<PAGE_SIZE>& alloc)
    -> bool
{
    auto set_header = vector::read(h.vector_header, u, alloc);
    return set::find(set_header, v, alloc) != set::end(set_header, alloc);
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<void, Alloc, PAGE_SIZE>::out_neighbors(
    const header& h, uint32_t u, const Alloc<PAGE_SIZE>& alloc) -> vertex_stream
{
    auto set_header = vector::read(h.vector_header, u, alloc);

    for(auto it = set::begin(set_header, alloc);
        it != set::end(set_header, alloc); ++it)
    {
        co_yield static_cast<uint32_t>(*it);
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<void, Alloc, PAGE_SIZE>::in_neighbors(
    const header& h, uint32_t v, const Alloc<PAGE_SIZE>& alloc) -> vertex_stream
{
    for(uint32_t u = 0; u < h.n; ++u)
    {
        auto set_header = vector::read(h.vector_header, u, alloc);
        auto it = set::find(set_header, v, alloc);
        if(it != set::end(set_header, alloc))
        {
            co_yield u;
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<void, Alloc, PAGE_SIZE>::edges(const header& h,
                                             const Alloc<PAGE_SIZE>& alloc)
    -> edge_stream
{
    for(uint32_t u = 0; u < h.n; ++u)
    {
        auto set_header = vector::read(h.vector_header, u, alloc);

        for(auto it = set::begin(set_header, alloc);
            it != set::end(set_header, alloc); ++it)
        {
            co_yield {u, *it};
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<void, Alloc, PAGE_SIZE>::out_degree(const header& h, uint32_t u,
                                                  const Alloc<PAGE_SIZE>& alloc)
    -> uint32_t
{
    auto set_header = vector::read(h.vector_header, u, alloc);
    return set_header.size;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<void, Alloc, PAGE_SIZE>::in_degree(const header& h, uint32_t v,
                                                 const Alloc<PAGE_SIZE>& alloc)
    -> uint32_t
{
    uint32_t degree = 0;
    for(const auto& u: in_neighbors(h, v, alloc))
    {
        ++degree;
    }

    return degree;
}

} // namespace graph_disk_structures
