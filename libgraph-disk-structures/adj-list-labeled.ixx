namespace graph_disk_structures
{
template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<Label, Alloc, PAGE_SIZE>::create(uint32_t n,
                                               Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = {};
    h.vector_header = vector::create(n, alloc);
    h.n = n;
    h.m = 0;

    for(uint32_t u = 0; u < n; ++u)
    {
        auto map_header = map::create(alloc);
        vector::write(h.vector_header, u, map_header, alloc);
    }

    return h;
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<Label, Alloc, PAGE_SIZE>::destroy(header* h,
                                                Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t u = 0; u < h->n; ++u)
    {
        auto map_header = vector::read(h->vector_header, u, *alloc);
        map::destroy(&map_header, alloc);
    }

#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif

    vector::destroy(&h->vector_header, alloc);
    h->n = 0;
    h->m = 0;
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<Label, Alloc, PAGE_SIZE>::clear(header* h,
                                              Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t u = 0; u < h->n; ++u)
    {
        auto map_header = vector::read(h->vector_header, u, *alloc);
        map::clear(&map_header, alloc);
        vector::write(h->vector_header, u, map_header, alloc);
    }

#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif

    h->m = 0;
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<Label, Alloc, PAGE_SIZE>::insert_edge(header* h, uint32_t u,
                                                    uint32_t v,
                                                    const Label& label,
                                                    Alloc<PAGE_SIZE>* alloc)
{
    auto map_header = vector::read(h->vector_header, u, *alloc);

#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    if(map::insert(&map_header, v, label, alloc))
    {
        ++h->m;
        vector::write(h->vector_header, u, map_header, alloc);
    }
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<Label, Alloc, PAGE_SIZE>::insert_or_update_edge(
    header* h, uint32_t u, uint32_t v, const Label& label,
    Alloc<PAGE_SIZE>* alloc)
{
    auto map_header = vector::read(h->vector_header, u, *alloc);

#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    if(!map::insert_or_update(&map_header, v, label, alloc))
    {
        ++h->m;
    }

    vector::write(h->vector_header, u, map_header, alloc);
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<Label, Alloc, PAGE_SIZE>::delete_edge(header* h, uint32_t u,
                                                    uint32_t v,
                                                    Alloc<PAGE_SIZE>* alloc)
{
    auto map_header = vector::read(h->vector_header, u, *alloc);

#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    if(map::remove(&map_header, v, alloc))
    {
        --h->m;
        vector::write(h->vector_header, u, map_header, alloc);
    }
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<Label, Alloc, PAGE_SIZE>::delete_out_neighbors(
    header* h, uint32_t u, Alloc<PAGE_SIZE>* alloc)
{
    auto map_header = vector::read(h->vector_header, u, *alloc);

    if(map_header.size > 0)
    {
        h->m -= map_header.size;
        map::clear(&map_header, alloc);
        vector::write(h->vector_header, u, map_header, alloc);
    }
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_list<Label, Alloc, PAGE_SIZE>::delete_in_neighbors(
    header* h, uint32_t v, Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t u = 0; u < h->n; ++u)
    {
        auto map_header = vector::read(h->vector_header, u, *alloc);

#ifdef BENCHMARK_ON
        ++total_binary_searches;
#endif
        if(map::remove(&map_header, v, alloc))
        {
            --h->m;
            vector::write(h->vector_header, u, map_header, alloc);
        }
    }
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<Label, Alloc, PAGE_SIZE>::has_edge(const header& h, uint32_t u,
                                                 uint32_t v,
                                                 const Alloc<PAGE_SIZE>& alloc)
    -> bool
{
    auto map_header = vector::read(h.vector_header, u, alloc);
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    return map::find(map_header, v, alloc) != map::end(map_header, alloc);
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<Label, Alloc, PAGE_SIZE>::label(const header& h, uint32_t u,
                                              uint32_t v,
                                              const Alloc<PAGE_SIZE>& alloc)
    -> std::optional<Label>
{
    auto map_header = vector::read(h.vector_header, u, alloc);
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    auto it = map::find(map_header, v, alloc);
    return it != map::end(map_header, alloc) ? std::make_optional(it->second)
                                             : std::nullopt;
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<Label, Alloc, PAGE_SIZE>::out_neighbors(
    const header& h, uint32_t u, const Alloc<PAGE_SIZE>& alloc) -> vertex_stream
{
    auto map_header = vector::read(h.vector_header, u, alloc);

    for(auto it = map::begin(map_header, alloc);
        it != map::end(map_header, alloc); ++it)
    {
        co_yield *it;
    }
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<Label, Alloc, PAGE_SIZE>::in_neighbors(
    const header& h, uint32_t v, const Alloc<PAGE_SIZE>& alloc) -> vertex_stream
{
    for(uint32_t u = 0; u < h.n; ++u)
    {
        auto map_header = vector::read(h.vector_header, u, alloc);
#ifdef BENCHMARK_ON
        ++total_binary_searches;
#endif
        auto it = map::find(map_header, v, alloc);
        if(it != map::end(map_header, alloc))
        {
            co_yield {u, it->second};
        }
    }
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<Label, Alloc, PAGE_SIZE>::edges(const header& h,
                                              const Alloc<PAGE_SIZE>& alloc)
    -> edge_stream
{
    for(uint32_t u = 0; u < h.n; ++u)
    {
        auto map_header = vector::read(h.vector_header, u, alloc);

        for(auto it = map::begin(map_header, alloc);
            it != map::end(map_header, alloc); ++it)
        {
            co_yield {u, it->first, it->second};
        }
    }
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<Label, Alloc, PAGE_SIZE>::out_degree(
    const header& h, uint32_t u, const Alloc<PAGE_SIZE>& alloc) -> uint32_t
{
    auto map_header = vector::read(h.vector_header, u, alloc);
    return map_header.size;
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_list<Label, Alloc, PAGE_SIZE>::in_degree(const header& h, uint32_t v,
                                                  const Alloc<PAGE_SIZE>& alloc)
    -> uint32_t
{
    uint32_t degree = 0;
    for(const auto& u: in_neighbors(h, v, alloc))
    {
        ++degree;
    }

    return degree;
}

} // namespace graph_disk_structures
