#include <libgraph-disk-structures/detail/utility.hxx>

namespace graph_disk_structures
{
template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<Label, Alloc, PAGE_SIZE>::create(uint32_t n,
                                                 Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = {};
    h.matrix_header = matrix::create(
        static_cast<uint64_t>(n) * static_cast<uint64_t>(n), alloc);
    h.n = n;
    h.m = 0;

    return h;
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<Label, Alloc, PAGE_SIZE>::destroy(header* h,
                                                  Alloc<PAGE_SIZE>* alloc)
{
    matrix::destroy(&h->matrix_header, alloc);
    h->n = 0;
    h->m = 0;
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<Label, Alloc, PAGE_SIZE>::clear(header* h,
                                                Alloc<PAGE_SIZE>* alloc)
{
    matrix::fill(h->matrix_header, std::nullopt, alloc);
    h->m = 0;
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<Label, Alloc, PAGE_SIZE>::insert_edge(header* h, uint32_t u,
                                                      uint32_t v,
                                                      const Label& label,
                                                      Alloc<PAGE_SIZE>* alloc)
{
    uint32_t index = detail::idx(u, v, h->n);
    if(!matrix::read(h->matrix_header, index, *alloc))
    {
        matrix::write(h->matrix_header, index, label, alloc);
        ++h->m;
    }
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<Label, Alloc, PAGE_SIZE>::insert_or_update_edge(
    header* h, uint32_t u, uint32_t v, const Label& label,
    Alloc<PAGE_SIZE>* alloc)
{
    uint32_t index = detail::idx(u, v, h->n);
    if(!matrix::read(h->matrix_header, index, *alloc))
    {
        ++h->m;
    }

    matrix::write(h->matrix_header, index, label, alloc);
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<Label, Alloc, PAGE_SIZE>::delete_edge(header* h, uint32_t u,
                                                      uint32_t v,
                                                      Alloc<PAGE_SIZE>* alloc)
{
    uint32_t index = detail::idx(u, v, h->n);
    if(matrix::read(h->matrix_header, index, *alloc))
    {
        matrix::write(h->matrix_header, index, std::nullopt, alloc);
        --h->m;
    }
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<Label, Alloc, PAGE_SIZE>::delete_out_neighbors(
    header* h, uint32_t u, Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t v = 0; v < h->n; ++v)
    {
        uint32_t index = detail::idx(u, v, h->n);
        if(matrix::read(h->matrix_header, index, *alloc))
        {
            matrix::write(h->matrix_header, index, std::nullopt, alloc);
            --h->m;
        }
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<Label, Alloc, PAGE_SIZE>::delete_in_neighbors(
    header* h, uint32_t v, Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t u = 0; u < h->n; ++u)
    {
        uint32_t index = detail::idx(u, v, h->n);
        if(matrix::read(h->matrix_header, index, *alloc))
        {
            matrix::write(h->matrix_header, index, std::nullopt, alloc);
            --h->m;
        }
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += h->n; // will jump h->n times in vector
#endif
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<Label, Alloc, PAGE_SIZE>::has_edge(
    const header& h, uint32_t u, uint32_t v, const Alloc<PAGE_SIZE>& alloc)
    -> bool
{
    auto label = matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc);
    return label.has_value();
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<Label, Alloc, PAGE_SIZE>::label(const header& h, uint32_t u,
                                                uint32_t v,
                                                const Alloc<PAGE_SIZE>& alloc)
    -> std::optional<Label>
{
    return matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc);
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<Label, Alloc, PAGE_SIZE>::out_neighbors(
    const header& h, uint32_t u, const Alloc<PAGE_SIZE>& alloc) -> vertex_stream
{
    for(uint32_t v = 0; v < h.n; ++v)
    {
        if(auto label =
               matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc))
        {
            co_yield {v, *label};
        }
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<Label, Alloc, PAGE_SIZE>::in_neighbors(
    const header& h, uint32_t v, const Alloc<PAGE_SIZE>& alloc) -> vertex_stream
{
    for(uint32_t u = 0; u < h.n; ++u)
    {
        if(auto label =
               matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc))
        {
            co_yield {u, *label};
        }
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += h.n; // will jump h->n times in vector
#endif
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<Label, Alloc, PAGE_SIZE>::edges(const header& h,
                                                const Alloc<PAGE_SIZE>& alloc)
    -> edge_stream
{
    for(uint32_t u = 0; u < h.n; ++u)
    {
        for(uint32_t v = 0; v < h.n; ++v)
        {
            if(auto label =
                   matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc))
            {
                co_yield {u, v, *label};
            }
        }
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += h.n;
#endif
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<Label, Alloc, PAGE_SIZE>::out_degree(
    const header& h, uint32_t u, const Alloc<PAGE_SIZE>& alloc) -> uint32_t
{
    uint32_t degree = 0;
    for(uint32_t v = 0; v < h.n; ++v)
    {
        if(matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc))
        {
            ++degree;
        }
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif

    return degree;
}

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<Label, Alloc, PAGE_SIZE>::in_degree(
    const header& h, uint32_t v, const Alloc<PAGE_SIZE>& alloc) -> uint32_t
{
    uint32_t degree = 0;
    for(uint32_t u = 0; u < h.n; ++u)
    {
        if(matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc))
        {
            ++degree;
        }
    }
#ifdef BENCHMARK_ON
    total_sequential_searches += h.n;
#endif

    return degree;
}

} // namespace graph_disk_structures
