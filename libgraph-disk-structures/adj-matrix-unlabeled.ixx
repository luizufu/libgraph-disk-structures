#include <libgraph-disk-structures/detail/utility.hxx>

namespace graph_disk_structures
{
template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<void, Alloc, PAGE_SIZE>::create(uint32_t n,
                                                Alloc<PAGE_SIZE>* alloc)
    -> header
{
    header h = {};
    h.matrix_header = matrix::create(
        static_cast<uint64_t>(n) * static_cast<uint64_t>(n), alloc);
    h.n = n;
    h.m = 0;

    return h;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<void, Alloc, PAGE_SIZE>::destroy(header* h,
                                                 Alloc<PAGE_SIZE>* alloc)
{
    matrix::destroy(&h->matrix_header, alloc);
    h->n = 0;
    h->m = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<void, Alloc, PAGE_SIZE>::clear(header* h,
                                               Alloc<PAGE_SIZE>* alloc)
{
    matrix::fill(h->matrix_header, false, alloc);
    h->m = 0;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<void, Alloc, PAGE_SIZE>::insert_edge(header* h, uint32_t u,
                                                     uint32_t v,
                                                     Alloc<PAGE_SIZE>* alloc)
{
    if(!matrix::read(h->matrix_header, detail::idx(u, v, h->n), *alloc))
    {
        matrix::write(h->matrix_header, detail::idx(u, v, h->n), true, alloc);
        ++h->m;
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<void, Alloc, PAGE_SIZE>::delete_edge(header* h, uint32_t u,
                                                     uint32_t v,
                                                     Alloc<PAGE_SIZE>* alloc)
{
    if(matrix::read(h->matrix_header, detail::idx(u, v, h->n), *alloc))
    {
        matrix::write(h->matrix_header, detail::idx(u, v, h->n), false, alloc);
        --h->m;
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<void, Alloc, PAGE_SIZE>::delete_out_neighbors(
    header* h, uint32_t u, Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t v = 0; v < h->n; ++v)
    {
        if(matrix::read(h->matrix_header, detail::idx(u, v, h->n), *alloc))
        {
            matrix::write(h->matrix_header, detail::idx(u, v, h->n), false,
                          alloc);
            --h->m;
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
void adj_matrix<void, Alloc, PAGE_SIZE>::delete_in_neighbors(
    header* h, uint32_t v, Alloc<PAGE_SIZE>* alloc)
{
    for(uint32_t u = 0; u < h->n; ++u)
    {
        if(matrix::read(h->matrix_header, detail::idx(u, v, h->n), *alloc))
        {
            matrix::write(h->matrix_header, detail::idx(u, v, h->n), false,
                          alloc);
            --h->m;
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<void, Alloc, PAGE_SIZE>::has_edge(const header& h, uint32_t u,
                                                  uint32_t v,
                                                  const Alloc<PAGE_SIZE>& alloc)
    -> bool
{
    return matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc);
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<void, Alloc, PAGE_SIZE>::out_neighbors(
    const header& h, uint32_t u, const Alloc<PAGE_SIZE>& alloc) -> vertex_stream
{
    for(uint32_t v = 0; v < h.n; ++v)
    {
        if(matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc))
        {
            co_yield v;
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<void, Alloc, PAGE_SIZE>::in_neighbors(
    const header& h, uint32_t v, const Alloc<PAGE_SIZE>& alloc) -> vertex_stream
{
    for(uint32_t u = 0; u < h.n; ++u)
    {
        if(matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc))
        {
            co_yield u;
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<void, Alloc, PAGE_SIZE>::edges(const header& h,
                                               const Alloc<PAGE_SIZE>& alloc)
    -> edge_stream
{
    for(uint32_t u = 0; u < h.n; ++u)
    {
        for(uint32_t v = 0; v < h.n; ++v)
        {
            if(matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc))
            {
                co_yield {u, v};
            }
        }
    }
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<void, Alloc, PAGE_SIZE>::out_degree(
    const header& h, uint32_t u, const Alloc<PAGE_SIZE>& alloc) -> uint32_t
{
    uint32_t degree = 0;
    for(uint32_t v = 0; v < h.n; ++v)
    {
        if(matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc))
        {
            ++degree;
        }
    }

    return degree;
}

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
auto adj_matrix<void, Alloc, PAGE_SIZE>::in_degree(
    const header& h, uint32_t v, const Alloc<PAGE_SIZE>& alloc) -> uint32_t
{
    uint32_t degree = 0;
    for(uint32_t u = 0; u < h.n; ++u)
    {
        if(matrix::read(h.matrix_header, detail::idx(u, v, h.n), alloc))
        {
            ++degree;
        }
    }

    return degree;
}

} // namespace graph_disk_structures
