#pragma once

#include <bits/stdint-uintn.h>
namespace graph_disk_structures::detail
{
static auto idx(uint64_t i, uint64_t j, uint64_t n) -> uint64_t
{
    return i * n + j;
}

} // namespace graph_disk_structures::detail
