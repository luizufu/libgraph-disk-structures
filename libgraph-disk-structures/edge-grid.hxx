#pragma once

#include <cppcoro/generator.hpp>
#include <libdisk/map.hxx>
#include <libdisk/set.hxx>
#include <libdisk/vector.hxx>
#include <memory>

namespace graph_disk_structures
{
template<typename Label, template<size_t> typename Alloc,
         size_t PAGE_SIZE = 4096>
class edge_grid;

template<template<size_t> typename Alloc, size_t PAGE_SIZE>
class edge_grid<void, Alloc, PAGE_SIZE>
{
    struct edge
    {
        uint32_t u = 0;
        uint32_t v = 0;

        auto operator==(const edge& other) const -> bool
        {
            return u == other.u && v == other.v;
        }

        auto operator<(const edge& other) const -> bool
        {
            return std::make_pair(u, v) < std::make_pair(other.u, other.v);
        }
    };

    using set = disk::set<edge, Alloc, std::less<edge>, PAGE_SIZE>;
    using grid = disk::vector<typename set::header, Alloc, PAGE_SIZE>;

public:
    using vertex_stream = cppcoro::generator<uint32_t>;
    using edge_stream = cppcoro::generator<std::pair<uint32_t, uint32_t>>;

    struct header
    {
        typename grid::header grid_header = {};
        uint32_t n_grid = 0;
        uint32_t block_size = 32;
        uint32_t n = 0;
        uint32_t m = 0;
    };

    static auto create(uint32_t n, uint32_t block_size, Alloc<PAGE_SIZE>* alloc)
        -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void insert_edge(header* h, uint32_t u, uint32_t v,
                            Alloc<PAGE_SIZE>* alloc);
    static void delete_edge(header* h, uint32_t u, uint32_t v,
                            Alloc<PAGE_SIZE>* alloc);

    static void delete_out_neighbors(header* h, uint32_t u,
                                     Alloc<PAGE_SIZE>* alloc);
    static void delete_in_neighbors(header* h, uint32_t v,
                                    Alloc<PAGE_SIZE>* alloc);

    static auto has_edge(const header& h, uint32_t u, uint32_t v,
                         const Alloc<PAGE_SIZE>& alloc) -> bool;

    static auto out_neighbors(const header& h, uint32_t u,
                              const Alloc<PAGE_SIZE>& alloc) -> vertex_stream;
    static auto in_neighbors(const header& h, uint32_t v,
                             const Alloc<PAGE_SIZE>& alloc) -> vertex_stream;
    static auto edges(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> edge_stream;

    static auto out_degree(const header& h, uint32_t u,
                           const Alloc<PAGE_SIZE>& alloc) -> uint32_t;
    static auto in_degree(const header& h, uint32_t v,
                          const Alloc<PAGE_SIZE>& alloc) -> uint32_t;
};

template<typename Label, template<size_t> typename Alloc, size_t PAGE_SIZE>
class edge_grid
{
    struct edge
    {
        uint32_t u = 0;
        uint32_t v = 0;

        auto operator==(const edge& other) const -> bool
        {
            return u == other.u && v == other.v;
        }

        auto operator<(const edge& other) const -> bool
        {
            return std::make_pair(u, v) < std::make_pair(other.u, other.v);
        }
    };

    using map = disk::map<edge, Label, Alloc, std::less<edge>, PAGE_SIZE>;
    using grid = disk::vector<typename map::header, Alloc, PAGE_SIZE>;

public:
    using vertex_stream = cppcoro::generator<std::pair<uint32_t, Label>>;
    using edge_stream =
        cppcoro::generator<std::tuple<uint32_t, uint32_t, Label>>;

    struct header
    {
        typename grid::header grid_header = {};
        uint32_t n_grid = 0;
        uint32_t block_size = 32;
        uint32_t n = 0;
        uint32_t m = 0;
    };

    static auto create(uint32_t n, uint32_t block_size, Alloc<PAGE_SIZE>* alloc)
        -> header;
    static void destroy(header* h, Alloc<PAGE_SIZE>* alloc);
    static void clear(header* h, Alloc<PAGE_SIZE>* alloc);

    static void insert_edge(header* h, uint32_t u, uint32_t v,
                            const Label& label, Alloc<PAGE_SIZE>* alloc);
    static void insert_or_update_edge(header* h, uint32_t u, uint32_t v,
                                      const Label& label,
                                      Alloc<PAGE_SIZE>* alloc);
    static void delete_edge(header* h, uint32_t u, uint32_t v,
                            Alloc<PAGE_SIZE>* alloc);

    static void delete_out_neighbors(header* h, uint32_t u,
                                     Alloc<PAGE_SIZE>* alloc);
    static void delete_in_neighbors(header* h, uint32_t v,
                                    Alloc<PAGE_SIZE>* alloc);

    static auto has_edge(const header& h, uint32_t u, uint32_t v,
                         const Alloc<PAGE_SIZE>& alloc) -> bool;

    static auto label(const header& h, uint32_t u, uint32_t v,
                      const Alloc<PAGE_SIZE>& alloc) -> std::optional<Label>;

    static auto out_neighbors(const header& h, uint32_t u,
                              const Alloc<PAGE_SIZE>& alloc) -> vertex_stream;
    static auto in_neighbors(const header& h, uint32_t v,
                             const Alloc<PAGE_SIZE>& alloc) -> vertex_stream;
    static auto edges(const header& h, const Alloc<PAGE_SIZE>& alloc)
        -> edge_stream;

    static auto out_degree(const header& h, uint32_t u,
                           const Alloc<PAGE_SIZE>& alloc) -> uint32_t;
    static auto in_degree(const header& h, uint32_t v,
                          const Alloc<PAGE_SIZE>& alloc) -> uint32_t;

#ifdef BENCHMARK_ON
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
#endif
};

} // namespace graph_disk_structures

#include <libgraph-disk-structures/edge-grid-labeled.ixx>
#include <libgraph-disk-structures/edge-grid-unlabeled.ixx>
