#include <algorithm>
#include <cassert>
#include <cppcoro/generator.hpp>
#include <filesystem>
#include <iostream>
#include <libdisk/allocator.hxx>
#include <libdisk/buffered-allocator.hxx>
#include <libgraph-disk-structures/adj-list.hxx>
#include <libgraph-disk-structures/adj-matrix.hxx>
#include <libgraph-disk-structures/edge-grid.hxx>
#include <set>
#include <string_view>
#include <vector>

using namespace graph_disk_structures;

template<template<typename, template<size_t> typename, size_t> typename G,
         template<size_t> typename Alloc, size_t PAGE_SIZE>
void test_edge_unlabeled();

template<template<typename, template<size_t> typename, size_t> typename G,
         template<size_t> typename Alloc, size_t PAGE_SIZE>
void test_edge_labeled();

auto main() -> int
{
    test_edge_unlabeled<adj_list, disk::allocator, 256>();
    test_edge_unlabeled<adj_matrix, disk::allocator, 256>();
    test_edge_unlabeled<edge_grid, disk::allocator, 256>();
    test_edge_labeled<adj_list, disk::allocator, 256>();
    test_edge_labeled<adj_matrix, disk::allocator, 256>();
    test_edge_labeled<edge_grid, disk::allocator, 256>();

    test_edge_unlabeled<adj_list, disk::buffered_allocator, 256>();
    test_edge_unlabeled<adj_matrix, disk::buffered_allocator, 256>();
    test_edge_unlabeled<edge_grid, disk::buffered_allocator, 256>();
    test_edge_labeled<adj_list, disk::buffered_allocator, 256>();
    test_edge_labeled<adj_matrix, disk::buffered_allocator, 256>();
    test_edge_labeled<edge_grid, disk::buffered_allocator, 256>();
}

template<typename T>
auto gen_has(cppcoro::generator<T> gen, std::set<T>&& values) -> bool
{
    for(const auto& x: gen)
    {
        if(values.count(x) == 0)
        {
            return false;
        }
    }

    return true;
}

template<typename T>
constexpr auto type_name() noexcept
{
    std::string_view name = "Error: unsupported compiler";
    std::string_view prefix;
    std::string_view suffix;
#ifdef __clang__
    name = __PRETTY_FUNCTION__;
    prefix = "auto type_name() [T = ";
    suffix = "]";
#elif defined(__GNUC__)
    name = __PRETTY_FUNCTION__;
    prefix = "constexpr auto type_name() [with T = ";
    suffix = "]";
#elif defined(_MSC_VER)
    name = __FUNCSIG__;
    prefix = "auto __cdecl type_name<";
    suffix = ">(void) noexcept";
#endif
    name.remove_prefix(prefix.size());
    name.remove_suffix(suffix.size());
    return name;
}

template<template<typename, template<size_t> typename, size_t> typename G,
         template<size_t> typename Alloc, size_t PAGE_SIZE>
void test_edge_unlabeled()
{
    using Graph = G<void, Alloc, PAGE_SIZE>;
    std::cout << "Testing " << type_name<Graph>() << std::endl;

    std::filesystem::remove("graph.bin");
    Alloc<PAGE_SIZE> alloc("graph.bin", 0);

    typename Graph::header h = {};

    if constexpr(!std::is_same_v<Graph, edge_grid<void, Alloc, PAGE_SIZE>>)
    {
        h = Graph::create(3, &alloc);
    }
    else
    {
        h = Graph::create(3, 32U, &alloc);
    }

    {
        assert(h.n == 3);
        assert(h.m == 0);
    }

    {
        Graph::insert_edge(&h, 0, 2, &alloc);
        Graph::insert_edge(&h, 1, 2, &alloc);
        Graph::insert_edge(&h, 1, 0, &alloc);
        Graph::insert_edge(&h, 1, 1, &alloc);
        Graph::insert_edge(&h, 2, 2, &alloc);

        assert(h.m == 5);

        assert(Graph::out_degree(h, 0, alloc) == 1);
        assert(Graph::out_degree(h, 1, alloc) == 3);
        assert(Graph::out_degree(h, 2, alloc) == 1);

        assert(Graph::in_degree(h, 0, alloc) == 1);
        assert(Graph::in_degree(h, 1, alloc) == 1);
        assert(Graph::in_degree(h, 2, alloc) == 3);
    }

    {
        assert(gen_has(Graph::edges(h, alloc),
                       {{0, 2}, {1, 2}, {1, 0}, {1, 1}, {2, 2}}));

        assert(gen_has(Graph::out_neighbors(h, 0, alloc), {2}));
        assert(gen_has(Graph::out_neighbors(h, 1, alloc), {2, 0, 1}));
        assert(gen_has(Graph::out_neighbors(h, 2, alloc), {2}));

        assert(gen_has(Graph::in_neighbors(h, 0, alloc), {1}));
        assert(gen_has(Graph::in_neighbors(h, 1, alloc), {1}));
        assert(gen_has(Graph::in_neighbors(h, 2, alloc), {0, 1, 2}));
    }

    {
        Graph::delete_edge(&h, 1, 0, &alloc);
        assert(h.m == 4);

        Graph::delete_edge(&h, 1, 0, &alloc);
        assert(h.m == 4);

        Graph::delete_out_neighbors(&h, 1, &alloc);
        assert(h.m == 2);

        Graph::delete_out_neighbors(&h, 1, &alloc);
        assert(h.m == 2);

        Graph::delete_in_neighbors(&h, 2, &alloc);
        assert(h.m == 0);

        Graph::delete_in_neighbors(&h, 2, &alloc);
        assert(h.m == 0);
    }

    Graph::destroy(&h, &alloc);
    alloc.clear();
    std::filesystem::remove("graph.bin");
}

template<template<typename, template<size_t> typename, size_t> typename G,
         template<size_t> typename Alloc, size_t PAGE_SIZE>
void test_edge_labeled()
{
    using Graph = G<uint32_t, Alloc, PAGE_SIZE>;
    std::cout << "Testing " << type_name<Graph>() << std::endl;

    std::filesystem::remove("graph.bin");
    Alloc<PAGE_SIZE> alloc("graph.bin", 0);

    typename Graph::header h = {};

    if constexpr(!std::is_same_v<Graph, edge_grid<uint32_t, Alloc, PAGE_SIZE>>)
    {
        h = Graph::create(3, &alloc);
    }
    else
    {
        h = Graph::create(3, 32U, &alloc);
    }

    {
        assert(h.n == 3);
        assert(h.m == 0);
    }

    {
        Graph::insert_edge(&h, 0, 2, 1, &alloc);
        Graph::insert_edge(&h, 1, 2, 2, &alloc);
        Graph::insert_edge(&h, 1, 0, 3, &alloc);
        Graph::insert_edge(&h, 1, 1, 4, &alloc);
        Graph::insert_edge(&h, 2, 2, 5, &alloc);

        assert(h.m == 5);

        assert(Graph::out_degree(h, 0, alloc) == 1);
        assert(Graph::out_degree(h, 1, alloc) == 3);
        assert(Graph::out_degree(h, 2, alloc) == 1);

        assert(Graph::in_degree(h, 0, alloc) == 1);
        assert(Graph::in_degree(h, 1, alloc) == 1);
        assert(Graph::in_degree(h, 2, alloc) == 3);

        assert(*Graph::label(h, 0, 2, alloc) == 1);
        assert(*Graph::label(h, 1, 2, alloc) == 2);
        assert(*Graph::label(h, 1, 0, alloc) == 3);
        assert(*Graph::label(h, 1, 1, alloc) == 4);
        assert(*Graph::label(h, 2, 2, alloc) == 5);
    }

    {
        assert(
            gen_has(Graph::edges(h, alloc),
                    {{0, 2, 1}, {1, 2, 2}, {1, 0, 3}, {1, 1, 4}, {2, 2, 5}}));
        assert(gen_has(Graph::out_neighbors(h, 0, alloc), {{2, 1}}));
        assert(gen_has(Graph::out_neighbors(h, 1, alloc),
                       {{2, 2}, {0, 3}, {1, 4}}));
        assert(gen_has(Graph::out_neighbors(h, 2, alloc), {{2, 5}}));

        assert(gen_has(Graph::in_neighbors(h, 0, alloc), {{1, 3}}));
        assert(gen_has(Graph::in_neighbors(h, 1, alloc), {{1, 4}}));
        assert(gen_has(Graph::in_neighbors(h, 2, alloc),
                       {{0, 1}, {1, 2}, {2, 5}}));
    }

    {
        Graph::delete_edge(&h, 1, 0, &alloc);
        assert(h.m == 4);

        Graph::delete_edge(&h, 1, 0, &alloc);
        assert(h.m == 4);

        Graph::delete_out_neighbors(&h, 1, &alloc);
        assert(h.m == 2);

        Graph::delete_out_neighbors(&h, 1, &alloc);
        assert(h.m == 2);

        Graph::delete_in_neighbors(&h, 2, &alloc);
        assert(h.m == 0);

        Graph::delete_in_neighbors(&h, 2, &alloc);
        assert(h.m == 0);
    }

    Graph::destroy(&h, &alloc);
    alloc.clear();
    std::filesystem::remove("graph.bin");
}
