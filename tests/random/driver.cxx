#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>
#include <cassert>
#include <filesystem>
#include <iostream>
#include <libdisk/allocator.hxx>
#include <libdisk/buffered-allocator.hxx>
#include <libgraph-disk-structures/adj-list.hxx>
#include <libgraph-disk-structures/adj-matrix.hxx>
#include <libgraph-disk-structures/edge-grid.hxx>
#include <random>
#include <vector>

using namespace graph_disk_structures;

template<template<typename, template<size_t> typename, size_t> typename G,
         template<size_t> typename Alloc, size_t PAGE_SIZE>
void test_edge_unlabeled(size_t n);

template<template<typename, template<size_t> typename, size_t> typename G,
         template<size_t> typename Alloc, size_t PAGE_SIZE>
void test_edge_labeled(size_t n);

auto main() -> int
{
    test_edge_unlabeled<adj_list, disk::allocator, 256>(1024);
    test_edge_unlabeled<adj_matrix, disk::allocator, 256>(1024);
    test_edge_unlabeled<edge_grid, disk::allocator, 256>(1024);
    test_edge_labeled<adj_list, disk::allocator, 256>(1024);
    test_edge_labeled<adj_matrix, disk::allocator, 256>(1024);
    test_edge_labeled<edge_grid, disk::allocator, 256>(1024);

    test_edge_unlabeled<adj_list, disk::buffered_allocator, 256>(1024);
    test_edge_unlabeled<adj_matrix, disk::buffered_allocator, 256>(1024);
    test_edge_unlabeled<edge_grid, disk::buffered_allocator, 256>(1024);
    test_edge_labeled<adj_list, disk::buffered_allocator, 256>(1024);
    test_edge_labeled<adj_matrix, disk::buffered_allocator, 256>(1024);
    test_edge_labeled<edge_grid, disk::buffered_allocator, 256>(1024);
}

auto next_probability() -> float
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r(), r(), r(), r(), r(), r()};
    static std::mt19937 gen(seed);
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(gen);
}

using edge = std::pair<uint32_t, uint32_t>;
using edge_stream = cppcoro::generator<edge>;

auto equals(const std::set<uint32_t>& s1, const std::set<uint32_t>& s2) -> bool
{
    auto it1 = s1.begin();
    auto end1 = s1.end();
    auto it2 = s2.begin();
    auto end2 = s2.end();

    for(; it1 != end1 && it2 != end2; ++it1, ++it2)
    {
        if(*it1 != *it2)
        {
            return false;
        }
    }

    return it1 == end1 && it2 == end2;
}

auto make_gnp(uint32_t n, float p) -> edge_stream
{
    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = 0; v < n; ++v)
        {
            if(next_probability() < p)
            {
                co_yield {u, v};
            }
        }
    }
}

template<typename T>
constexpr auto type_name() noexcept
{
    std::string_view name = "Error: unsupported compiler";
    std::string_view prefix;
    std::string_view suffix;
#ifdef __clang__
    name = __PRETTY_FUNCTION__;
    prefix = "auto type_name() [T = ";
    suffix = "]";
#elif defined(__GNUC__)
    name = __PRETTY_FUNCTION__;
    prefix = "constexpr auto type_name() [with T = ";
    suffix = "]";
#elif defined(_MSC_VER)
    name = __FUNCSIG__;
    prefix = "auto __cdecl type_name<";
    suffix = ">(void) noexcept";
#endif
    name.remove_prefix(prefix.size());
    name.remove_suffix(suffix.size());
    return name;
}

template<template<typename, template<size_t> typename, size_t> typename G,
         template<size_t> typename Alloc, size_t PAGE_SIZE>
void test_edge_unlabeled(size_t n)
{
    using Graph = G<void, Alloc, PAGE_SIZE>;
    std::cout << "Testing " << type_name<Graph>() << std::endl;

    boost::adjacency_list<boost::setS, boost::vecS, boost::bidirectionalS> gb;
    std::vector<edge> not_in_graph;
    std::vector<edge> samples_in_graph;

    std::filesystem::remove("graph_random.bin");
    Alloc<PAGE_SIZE> alloc("graph_random.bin", 0);

    typename Graph::header h = {};

    if constexpr(!std::is_same_v<Graph, edge_grid<void, Alloc, PAGE_SIZE>>)
    {
        h = Graph::create(n, &alloc);
    }
    else
    {
        h = Graph::create(n, 32U, &alloc);
    }

    {
        for(auto e: make_gnp(n, 0.3F))
        {
            if(next_probability() < 0.3F)
            {
                not_in_graph.push_back(e);
            }
            else
            {
                Graph::insert_edge(&h, e.first, e.second, &alloc);
                boost::add_edge(e.first, e.second, gb);

                if(next_probability() < 0.3F)
                {
                    samples_in_graph.push_back(e);
                }
            }
        }
    }

    {
        assert(boost::num_vertices(gb) == h.n);
        assert(boost::num_edges(gb) == h.m);

        for(auto e: samples_in_graph)
        {
            assert(Graph::has_edge(h, e.first, e.second, alloc)
                   == boost::edge(e.first, e.second, gb).second);
        }

        for(auto e: not_in_graph)
        {
            assert(Graph::has_edge(h, e.first, e.second, alloc)
                   == boost::edge(e.first, e.second, gb).second);
        }
    }

    {
        std::vector<uint32_t> sample_vertices;

        for(uint32_t u = 0; u < n; ++u)
        {
            {
                assert(boost::out_degree(u, gb)
                       == Graph::out_degree(h, u, alloc));

                std::set<uint32_t> g_neighbors;
                std::set<uint32_t> boost_neighbors;

                for(uint32_t v: Graph::out_neighbors(h, u, alloc))
                {
                    g_neighbors.insert(v);
                }

                for(auto [it2, end2] = boost::out_edges(u, gb); it2 != end2;
                    ++it2)
                {
                    boost_neighbors.insert(it2->m_target);
                }

                assert(equals(g_neighbors, boost_neighbors));
            }

            {
                assert(boost::in_degree(u, gb)
                       == Graph::in_degree(h, u, alloc));

                std::set<uint32_t> g_neighbors;
                std::set<uint32_t> boost_neighbors;

                for(uint32_t v: Graph::in_neighbors(h, u, alloc))
                {
                    g_neighbors.insert(v);
                }

                for(auto [it2, end2] = boost::in_edges(u, gb); it2 != end2;
                    ++it2)
                {
                    boost_neighbors.insert(it2->m_source);
                }

                assert(equals(g_neighbors, boost_neighbors));
            }

            if(u % static_cast<uint32_t>(n * 0.1F) == 0)
            {
                sample_vertices.push_back(u);
            }
        }

        bool in_out = false;

        for(auto u: sample_vertices)
        {
            if(in_out)
            {
                Graph::delete_in_neighbors(&h, u, &alloc);
                boost::remove_in_edge_if(
                    u,
                    [](auto /*unused*/)
                    {
                        return true;
                    },
                    gb);

                assert(Graph::in_degree(h, u, alloc) == 0);
            }
            else
            {
                Graph::delete_out_neighbors(&h, u, &alloc);
                boost::remove_out_edge_if(
                    u,
                    [](auto /*unused*/)
                    {
                        return true;
                    },
                    gb);

                assert(Graph::out_degree(h, u, alloc) == 0);
            }

            in_out = !in_out;

            assert(boost::num_vertices(gb) == h.n);
            assert(boost::num_edges(gb) == h.m);
        }

        for(auto e: samples_in_graph)
        {
            Graph::delete_edge(&h, e.first, e.second, &alloc);
            boost::remove_edge(e.first, e.second, gb);
            assert(boost::num_edges(gb) == h.m);
        }

        for(auto e: samples_in_graph)
        {
            assert(Graph::has_edge(h, e.first, e.second, alloc)
                   == boost::edge(e.first, e.second, gb).second);
        }

        for(auto [it, end] = boost::edges(gb); it != end; ++it)
        {
            assert(Graph::has_edge(h, it->m_source, it->m_target, alloc));
        }
    }

    Graph::destroy(&h, &alloc);
    alloc.clear();
    std::filesystem::remove("graph_random.bin");
}

template<template<typename, template<size_t> typename, size_t> typename G,
         template<size_t> typename Alloc, size_t PAGE_SIZE>
void test_edge_labeled(size_t n)
{
    using Graph = G<uint32_t, Alloc, PAGE_SIZE>;
    std::cout << "Testing " << type_name<Graph>() << std::endl;

    using boost_graph =
        boost::adjacency_list<boost::setS, boost::vecS, boost::bidirectionalS,
                              boost::no_property,
                              boost::property<boost::edge_name_t, uint32_t>>;
    boost_graph gb;
    std::vector<edge> not_in_graph;
    std::vector<edge> samples_in_graph;

    std::filesystem::remove("graph_random.bin");
    Alloc<PAGE_SIZE> alloc("graph_random.bin", 0);

    typename Graph::header h = {};

    if constexpr(!std::is_same_v<Graph, edge_grid<uint32_t, Alloc, PAGE_SIZE>>)
    {
        h = Graph::create(n, &alloc);
    }
    else
    {
        h = Graph::create(n, 32U, &alloc);
    }

    {
        for(auto e: make_gnp(n, 0.3F))
        {
            uint32_t label = next_probability() * 100;

            if(next_probability() < 0.3F)
            {
                not_in_graph.push_back(e);
            }
            else
            {
                Graph::insert_edge(&h, e.first, e.second, label, &alloc);
                boost::add_edge(e.first, e.second, label, gb);

                if(next_probability() < 0.3F)
                {
                    samples_in_graph.push_back(e);
                }
            }
        }
    }

    {
        assert(boost::num_vertices(gb) == h.n);
        assert(boost::num_edges(gb) == h.m);

        auto labelmap = boost::get(boost::edge_name, gb);

        for(auto e: samples_in_graph)
        {
            auto boost_edge = boost::edge(e.first, e.second, gb);
            assert(Graph::has_edge(h, e.first, e.second, alloc)
                   == boost_edge.second);
            assert(*Graph::label(h, e.first, e.second, alloc)
                   == labelmap[boost_edge.first]);
        }

        for(auto e: not_in_graph)
        {
            assert(Graph::has_edge(h, e.first, e.second, alloc)
                   == boost::edge(e.first, e.second, gb).second);
        }
    }

    {
        std::vector<uint32_t> sample_vertices;

        for(uint32_t u = 0; u < n; ++u)
        {
            {
                assert(boost::out_degree(u, gb)
                       == Graph::out_degree(h, u, alloc));

                std::set<uint32_t> g_neighbors;
                std::set<uint32_t> boost_neighbors;

                for(const auto& [v, label]: Graph::out_neighbors(h, u, alloc))
                {
                    g_neighbors.insert(v);
                }

                for(auto [it2, end2] = boost::out_edges(u, gb); it2 != end2;
                    ++it2)
                {
                    boost_neighbors.insert(it2->m_target);
                }

                assert(equals(g_neighbors, boost_neighbors));
            }

            {
                assert(boost::in_degree(u, gb)
                       == Graph::in_degree(h, u, alloc));

                std::set<uint32_t> g_neighbors;
                std::set<uint32_t> boost_neighbors;

                for(const auto& [v, label]: Graph::in_neighbors(h, u, alloc))
                {
                    g_neighbors.insert(v);
                }

                for(auto [it2, end2] = boost::in_edges(u, gb); it2 != end2;
                    ++it2)
                {
                    boost_neighbors.insert(it2->m_source);
                }

                assert(equals(g_neighbors, boost_neighbors));
            }

            if(u % static_cast<uint32_t>(n * 0.1F) == 0)
            {
                sample_vertices.push_back(u);
            }
        }

        bool in_out = false;

        for(auto u: sample_vertices)
        {
            if(in_out)
            {
                Graph::delete_in_neighbors(&h, u, &alloc);
                boost::remove_in_edge_if(
                    u,
                    [](auto /*unused*/)
                    {
                        return true;
                    },
                    gb);

                assert(Graph::in_degree(h, u, alloc) == 0);
            }
            else
            {
                Graph::delete_out_neighbors(&h, u, &alloc);
                boost::remove_out_edge_if(
                    u,
                    [](auto /*unused*/)
                    {
                        return true;
                    },
                    gb);

                assert(Graph::out_degree(h, u, alloc) == 0);
            }

            in_out = !in_out;

            assert(boost::num_vertices(gb) == h.n);
            assert(boost::num_edges(gb) == h.m);
        }

        for(auto e: samples_in_graph)
        {
            Graph::delete_edge(&h, e.first, e.second, &alloc);
            boost::remove_edge(e.first, e.second, gb);
            assert(boost::num_edges(gb) == h.m);
        }

        for(auto e: samples_in_graph)
        {
            assert(Graph::has_edge(h, e.first, e.second, alloc)
                   == boost::edge(e.first, e.second, gb).second);
        }

        for(auto [it, end] = boost::edges(gb); it != end; ++it)
        {
            assert(Graph::has_edge(h, it->m_source, it->m_target, alloc));
        }
    }

    Graph::destroy(&h, &alloc);
    alloc.clear();
    std::filesystem::remove("graph_random.bin");
}
