# libgraph-disk-structures

C++ library

## dependencies

cppcoro
libdisk (interfaced)
boost (for tests)


## after commands

    bdep init -C {build-out}/libgraph-disk-structures-clang @clang cc config.cxx=clang++ \
        config.cxx.coptions = -stdlib=libc++                             \
        config.cc.poptions=-I{build-unpkg}/include                       \
        config.cc.loptions=-L{build-unpkg}/lib

where {build-out} can be relative and {build-unpkg} not
